#!/usr/bin/env python3
#
# Flaws: currently just uses the first file in the reference list whose MD5 hash matches the unknown file; does not attempt to list every actual match (just the first).

import sys, os, subprocess, hashlib, shutil

import pdb
st = pdb.set_trace

def shell_open_file(filename):
    if sys.platform == "win32":
        os.startfile(filename)
    else:
        opener = "open" if sys.platform == "darwin" else "xdg-open"
        subprocess.call([opener, filename])

def cmfiles(src_folder, dest_folder, filter_fn, command):
    last_dest_folder = ""
    for fold in os.walk(src_folder):

        if len(fold[2]) is 0:
            continue

        path = fold[0]
        for f in fold[2]:
            if filter_fn(f) is not True:
                continue

            fpath = os.path.join(path, f)

            #shell_open_file(fpath)
            os.system(command + " " + fpath)
            new_folder = input("Move to (space to skip, return to use {:s}):".format(last_dest_folder))
            if new_folder is " ":
                continue
            elif new_folder is "":
                new_folder = last_dest_folder
            else:
                last_dest_folder = new_folder

            tgt_folder = os.path.join(dest_folder, new_folder)
            new_filename = input("New filename (return to leave unchanged):")
            if new_filename is "":
                new_filename = f

            if not os.path.exists(tgt_folder):
                os.makedirs(tgt_folder)

            newpath = os.path.join(tgt_folder, new_filename)        
            if not os.path.exists(newpath):
                print("Moving {:s} to {:s}".format(fpath, newpath))
                shutil.move(fpath, newpath)
            else:
                print("{:s} already exists! Skipping.".format(newpath))

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print(""" cmfiles opens a class of files sequentially and allows you to move and/or rename them into a folder of your choice. Should save time for repetitive operations.

Usage:
    cmfiles.py <src> <dest> <filter> <open_command>

        src: source folder to search for files
        dest: top-level destination folder to move the files to
        filter: files to include in the search
        open_command: command to view the files in to make a decision

    Both relative and absolute paths are fine.

Example:
    cmfiles.py /run/media/usbkey /run/media/backup .avi mplayer
""")
        exit()
        
    cmfiles(sys.argv[1], sys.argv[2], lambda k: sys.argv[3] in k, sys.argv[4])
    # if sys.argv[1] == "fl":
    #     folder = sys.argv[2]
    #     outfile = sys.argv[3]
    #     if len(sys.argv) > 4:
    #         filter_fn = lambda k: sys.argv[4] in k
    #     else:
    #         filter_fn = lambda k: True

    #     create_filelist(folder, filter_fn=filter_fn, outfile=outfile)

    # elif sys.argv[1] == "m":
    #     reference_files = sys.argv[2]
    #     new_files = sys.argv[3]
    #     output_file = sys.argv[4]
    #     unmatched_file = None
    #     if len(sys.argv) > 5:
    #         unmatched_file = sys.argv[5]

    #     fdr = read_filelist(reference_files)
    #     fdn = read_filelist(new_files)

    #     filematch(fdr, fdn, output_file, unmatched_file)

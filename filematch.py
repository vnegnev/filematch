#!/usr/bin/env python3
#
# Flaws: currently just uses the first file in the reference list whose MD5 hash matches the unknown file; does not attempt to list every actual match (just the first).

import sys, os, hashlib

import pdb
st = pdb.set_trace

verbose = True

hash_sha = lambda k: hashlib.sha256(open(k, 'rb').read()).hexdigest()
hash_size = lambda k: hex(os.path.getsize(k))

def create_filelist(folder, filter_fn=lambda k: True, outfile=None, hash_function=hash_size):
    """ filter_fn: Function which when given the filename will return True if the file is relevant. """
    if outfile is None:
        outfile = os.path.join(folder, "filelist.txt")

    filelist = []

    ## First, get all files in the folder
    for fold in os.walk(folder):
        # Skip folders
        if len(fold[2]) is 0:
            continue

        path = fold[0]
        for f in fold[2]:
            if filter_fn(f) is not True:
                continue

            fpath = os.path.join(path, f)
            filelist.append(hash_function(fpath) + "\t" + fpath + "\n")
            
            if verbose:
                if len(filelist) % 50 == 0:
                    print("{:d} files found".format(len(filelist)))

    with open(outfile, "w+") as flist:
        for l in filelist:
            flist.write(l)

def read_filelist(flpath):
    """ Create a dictionary of hash:filepath entries """
    fd = {}
    with open(flpath, "r") as fl:
        for l in fl:
            filehash, filepath = l.strip("\n").split("\t")
            fd.update({filehash: filepath})

    return fd
                
def filematch(reference_filedict, new_filedict, outfile_matches=None, outfile_remaining_new=None):
    if outfile_matches is None:
        outfile_matches = os.path.join("matches.txt")

    if outfile_remaining_new is not None:
        remaining_new_filedict = dict(new_filedict)
        
    if verbose:
        N = len(new_filedict)
        M = 500
    with open(outfile_matches, "w") as ofm:
        for k, filehash in enumerate(new_filedict):
            
            if k % M == 0:
                print("file {:d}/{:d}".format(k, N))
                
            try:
                refpath = reference_filedict[filehash]
                ofm.write(refpath + '\t' + new_filedict[filehash] + "\n")
                if outfile_remaining_new is not None:
                    remaining_new_filedict.pop(filehash)
            except KeyError:
                continue

    if outfile_remaining_new is not None:
        with open(outfile_remaining_new, "w") as ofrn:
            for filehash in remaining_new_filedict:
                ofrn.write(filehash + '\t' + remaining_new_filedict[filehash] + '\n')

def test_operation():
    create_filelist(os.path.join("test_folder", "reference"), hash_function=hash_sha)
    create_filelist(os.path.join("test_folder", "new"), hash_function=hash_sha)

    fdr = read_filelist(os.path.join("test_folder", "reference", "filelist.txt"))
    fdn = read_filelist(os.path.join("test_folder", "new", "filelist.txt"))

    filematch(fdr, fdn, "matches.txt", "remaining_new.txt")

if __name__ == "__main__":
    if sys.argv[1] == "fl":
        folder = sys.argv[2]
        outfile = sys.argv[3]
        if len(sys.argv) > 4:
            filter_fn = lambda k: sys.argv[4] in k
        else:
            filter_fn = lambda k: True

        create_filelist(folder, filter_fn=filter_fn, outfile=outfile)

    elif sys.argv[1] == "m":
        reference_files = sys.argv[2]
        new_files = sys.argv[3]
        output_file = sys.argv[4]
        unmatched_file = None
        if len(sys.argv) > 5:
            unmatched_file = sys.argv[5]

        fdr = read_filelist(reference_files)
        fdn = read_filelist(new_files)

        filematch(fdr, fdn, output_file, unmatched_file)

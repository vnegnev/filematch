Simple file matching program, which can be used to look at two folders and identify which files are present inside both.

I used it after dumping a hard disk and using Photorec, producing hundreds of jpeg files with random names. I wanted to see which of these weren't already in my existing backed-up photo folders.

Let's say my backup folder was in /run/media/vlad/backup_hd/photos and my disk-dump folder was in /run/media/vlad/backup_hd/disk_dump . First I make two file and size lists, filtering exclusively for jpg files in the disk dump, then I compare them:

python3 filematch.py fl /run/media/vlad/backup_hd/disk_dump newfiles.txt .jpg
python3 filematch.py fl /run/media/vlad/backup_hd/photos oldfiles.txt # there is no filter string (above it's .jpg; don't use that here since it's currently case-sensitive so .JPG files will get skipped)
python3 filematch.py m oldfiles.txt newfiles.txt matchfiles.txt

Now, matchfiles.txt is filled with lines like

/run/media/vlad/backup_hd/photos/DSC-S30/December 2002 to February 2003/DSC00092.JPG	/run/media/vlad/backup_hd/disk_dump/recup_dir.9/f90416432.jpg

which can be processed by other tools, e.g. to delete the duplicate files from the disk dump.

Note: in principle it's faster to call filematch with the m parameter with the longer file list given first; above oldfiles.txt has tens of thousands of files, while newfiles.txt has only a few hundred. This is because this way a large dictionary is queried a small number of times, instead of a small dictionary being queried a large number of times. I haven't tested filematch on higher quantities of files so YMMV.
